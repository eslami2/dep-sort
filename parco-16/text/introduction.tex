\section{Introduction}
\label{sec:intro}
Many scientific applications running on HPC systems are dealing with ever
increasing amount of data~\cite{scalable_io, parallel_io_practice}. In many of these applications most of the execution time is spent in I/O where large amounts of data are written to and/or read from storage layers~\cite{io_study}.
HPC systems offer fast interconnection networks
and flexible I/O infrastructure that makes them very attractive for 
Big Data analysis as well~\cite{hpc_meets_bigdata,idc_forecast,hbase_rdma,hdfs_rdma,memcached_rdma,cray_mr_over_lustre,intel_mr_over_lustre}. Large-scale machine learning and graph analytics
are just a few examples of such analytics arisen in HPC systems~\cite{ml_hpc, grap500}.

On the other hand, current HPC systems are not well-suited for emerging data-intensive applications. Not only is the computation rate of current multi-core/many-core
architectures greater
than the data access rate in storage devices, but also the improvements in
computation rate follow a faster trend compared to the improvements in data 
access rate~\cite{arch_book, exascale_roadmap}. This causes the so called ``I/O-wall'' problem in which the gap between
computation rate and data access rate grows continuously.

Extensive research is done to propose architectural improvements to the current 
HPC environments
for data-intensive applications. One proposed solution is the use of more suitable devices such as
solid-state drives (SSD) and phase-change memories (PCM) in the I/O hardware stack~\cite{ssd_hpc, pcm_hpc}. Although this solution reduces the gap between computation rate and data access rate, it still does not solve the I/O-wall problem.
Another proposed solution is the addition of computational units closer to the actual data~\cite{active_storage_parallel_io,active_disk, io_forwarder_ibm}. This solution enables decoupling and shipping data-intensive operations closer to data,
reducing data movement all the way across the network from storage layers to main computational units.
For most users, however, available computational units closer to data usually have limited capability
and lack flexibility in programming, making the second solution difficult to
exploit.

One promising system architecture proposed for data\-/intensive computing is the 
Decoupled Execution Paradigm (DEP)~\cite{dep,dep_presentation}. DEP decouples nodes into Data Nodes (DN) and Compute
Nodes (CN). CNs are attached via a fast network and do not have
any persistent storage device. These nodes are similar to main computational units in
commonly used current supercomputing architectures, and are suitable for 
computational intensive operations. DNs are usually  nodes extended
with local SSDs suitable for data-intensive operations. Data nodes are divided
further into Compute-side Data Nodes (CDN) and Storage-side Data Nodes (SDN). SDNs are
connected to storage devices with a fast network, and similarly, CDNs are connected to
CNs with a fast network. With these programmable special DNs, DEP offers 
generality and flexibility in data-intensive operations and competitive to other architectures performance.

In this paper we investigate the effectiveness of DEP, both theoretically 
and practically, through a well-known data\-/intensive kernel,
\textbf{disk-to-disk sorting}. Our algorithm is designed for 
efficient sorting of a data-set that is too large to fit into the
available aggregate RAM\footnote{Code and full report at: \href{github.com/heslami/dep-sort}{github/heslami/dep-sort}}. Our contributions are the following:
\vspace{-5pt}
\begin{itemize}
\item We introduce a new sorting algorithm under the context of DEP that effectively utilizes
      all components of the DEP system architecture. We demonstrate that the total execution time
      of our algorithm is equivalent to the aggregate time needed to read the entire
      data from disk and write it back to disk. 
      %In other words, we show that the 
      %actual computation (sorting) and reading/writing of intermediate data is almost
      %entirely overlapped with reading the input and writing the output.
\item We analytically model the proposed sorting algorithm supporting our
      design choices, and prove the accuracy of our model.
\item We demonstrate that our approach in DEP achieves 30\% better performance compared
      to the \textit{theoretically optimal} sorting algorithm running on the same testbed without exploiting the DEP architecture.
\end{itemize}
