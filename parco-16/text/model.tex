\section{Analytical Model}
\label{sec:model}
In this section we express the constraints as well as a simple performance model for the proposed algorithm. Our analysis is based on system and algorithm specific parameters shown in Table~\ref{table:tunablemodelparams} and Table~\ref{table:systemmodelparams}.

\subsection{Constraints}
The tunable parameters we defined in Table~\ref{table:tunablemodelparams} are subject to some constraints, which are imposed due to the memory availability in the system. As described in section~\ref{sec:algo}, each phase of the algorithm has specific memory requirements. Expressing these using our model parameters will give us the constraints they need to satisfy.

\subsubsection{Storage-side Data Nodes}
During the read phase a SDN requires three buffers of size SDN\_READ\_SIZE (one for asynchronous read from the disk, one for sorting the data already being read, and one for initial merging of sorted sequences of data), two of size SDN\_BUFFER\_SIZE (one for keeping the sorted in-memory data, and one for merging in-memory data with newly read data from the disk), and one of size SDN\_BUFFER\_SIZE $\times$ WCCR (to hold outgoing compressed data to CNs). Therefore, we can express the constraint as:
\begin{dmath*}
3 \times \texttt{SDN\_READ\_SIZE} + \left(2 + \texttt{WCCR}\right) \times \texttt{SDN\_BUFFER\_SIZE} \leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}
Additionally, during the write phase a SDN requires one buffer of size CN\_MERGE \_BUFFER\_W (for asynchronous write to the disk) and two of size CN\_MERGE \_BUFFER\_W $\times$ WCCR (for double-buffering non-blocking receive from CNs and decompression of received data). Therefore, we can express the constraint as:
\begin{dmath*}
\left(1 + 2 \times \texttt{WCCR}\right) \times \texttt{CN\_MERGE\_BUFFER\_W} \leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}

\subsubsection{Compute Nodes}
During the read phase a CN requires two buffers of size CN\_MERGE\_BUFFE- R\_R (one to hold the sorted in-memory data, and one to be used to hold result of merging sorted in-memory data with newly received data), one of size CN\_MERGE \_BUFFER $\times$ WCCR (to hold outgoing compressed data to the assigned CDN), two of size CN\_RECV\_BUFFER (one to be used during the decompression of received and one to be used to merge with sorted in-memory data), and two of size CN\_RECV\_BUFFER $\times$ WCCR (for double-buffering non-blocking receive and decompression of received data). Therefore, we can express the constraint as:
\begin{dmath*}
\left(2 + \texttt{WCCR}\right) \times \texttt{CN\_MERGE\_BUFFER\_R} +
2 \times \texttt{CN\_RECV\_BUFFER} \times \left(1 + \texttt{WCCR}\right)
\leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}
During the write phase a CN requires two buffers of size CN\_MERGE\_BUFFER \_W (one for holding the sorted in-memory data and once to use for merging), one of size CN\_MERGE\_BUFFER\_W $\times$ WCCR (to hold outgoing compressed data to the assigned SDN). Additionally, it requires one of size CHUNK\_SIZE (to hold the data after decompressing newly received data from the CDN) and two of size CHUNK\_SIZE $\times$ WCCR for each data series (for double-buffering non-blocking receive of data from the CDN and decompression process of that data). The corresponding constraint is:

\begin{dmath*}
\left(2 + \texttt{WCCR}\right) \times \texttt{CN\_MERGE\_BUFFER\_W} +
\texttt{NUM\_SERIES} \times \left(1 + 2 \times \texttt{WCCR}\right) \times \texttt{CHUNK\_SIZE}
\leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}

\subsubsection{Compute-side Data Nodes}
During the read phase a CDN requires a buffer of size CHUNK\_SIZE $\times$ WCCR per CN that is assigned to it, and an additional buffer of the same size for writing to the local file (double-buffering the non-blocking receive with write to the disk). The constraint is as follows.
\begin{dmath*}
\left(\frac{\texttt{NUM\_CN}}{\texttt{NUM\_CDN}} + 1\right) \times \texttt{CHUNK\_SIZE} \times \texttt{WCCR}
\leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}
During the write phase, the maximum memory that a CDN requires is a buffer of size CHUNK\_SIZE $\times$ WCCR per CN assigned to it (to hold the data read from the local disk). The corresponding constraint is
\begin{dmath*}
\frac{\texttt{NUM\_CN}}{\texttt{NUM\_CDN}} \times \texttt{CHUNK\_SIZE} \times \texttt{WCCR}
\leq \texttt{DRAM\_ALLOCATION}
\end{dmath*}

\subsection{Performance Modeling}
For the sake of simplicity in devising a performance model, we assume negligible network latency in our modeling as almost all messages in our algorithm are large.

\subsubsection{Storage-side Data Nodes: Read Phase}
For \textbf{SDNs} during read phase, there are four major operations:
\begin{itemize}
\item Reading a buffer of size SDN\_READ\_SIZE from the file system.
\item Sorting the read buffer and merging it with the in\-/memory buffer (with maximum size of SDN\_BUFFER\_SIZE). We used quicksort in parallel where each thread performs approximately 1.5 accesses per element. For merging, two memory accesses per element are required.
%\vspace{-6mm}
\item Compressing consecutive ranges of the in-memory sorted buffer.
%\vspace{-2mm}
\item Sending compressed ranges to the CNs.
\end{itemize}
%\RM
\begin{displaymath}
T_{read}=\texttt{PFS\_LATENCY}+\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{PFS\_BW}}
\end{displaymath}
\RM
\begin{dmath*}
T_{sort} = \left(1.5\times\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{CORES\_PER\_NODE}}log\left({\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{TYPE\_SIZE}\times\texttt{CORES\_PER\_NODE}}}\right)
+ 2\times\texttt{SDN\_READ\_SIZE} + 2\times\texttt{SDN\_BUFFER\_SIZE}\right)\times\frac{1}{ \texttt{MEMORY\_BW}}
\end{dmath*} 
\RM
\begin{dmath*}
T_{comp} = \frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{COMPRESSION\_BW}\times\texttt{CORES\_PER\_NODE}}
\end{dmath*}
\RM
\begin{dmath*}
T_{send} = \frac{\texttt{SDN\_BUFFER\_SIZE} \times \texttt{WCCR}}{\texttt{NETWORK\_BW}}
\end{dmath*}

Based on the algorithm, $T_{read}$ is overlapped with $T_{sort}$. Once the in-memory buffer is full (after $\frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{SDN\_READ\_SIZE}}$ rounds of reading/sorting), compression happens. All of these operations are overlapped with $T_{send}$. Therefore, the time it takes for an SDN from the moment it starts reading data to the moment it sends out messages to CNs (which we call SDN's gap) can be expressed as follows:
\begin{dmath*}
SDN_{gap}^{read} = max\left\{\frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{SDN\_READ\_SIZE}} \times max \left\{ T_{read}, T_{sort} \right\}
 + T_{comp}, T_{send}\right\}
\end{dmath*}
The total effective execution time of each SDN in read phase is the sum of all these gaps for all communication rounds where SDN sends data to CNs. Hence, the total effective execution time of SDNs can be expressed as:
\begin{dmath*}
T_{SDN}^{read}=\frac{\texttt{FILE\_SIZE}}{\texttt{NUM\_SDN}\times\texttt{SDN\_BUFFER\_SIZE}}\times SDN_{gap}^{read}
\end{dmath*}

\subsubsection{Compute Nodes: Read Phase}
For \textbf{CDNs} during read phase, there are five major operations:
\begin{itemize}
	\item Receiving a compressed sorted sequence from SDNs. The size of the received message is at most CN\_RECV\_BUFFER $\times$ WCCR. In order to estimate the time of this operation, we need to account for the time that the SDNs need to compute and send these messages, i.e. the $SDN_{gap}^{read}$ as defined earlier. Since a CN is expected to receive messages from all SDNs during read phase, we can assume that the effective delay due to the $SDN_{gap}^{read}$ is estimated by $\frac{SDN_{gap}^{read}}{\texttt{NUM\_SDN}}$. Note that $SDN_{gap}^{read}$ implicitly includes the time it takes to transfer the message through the network.
	\item Decompressing the received message into a buffer of size CN\_RECV\_BUF- FER.
	\item Merging the received sorted sequence with the in-memory sorted data into a buffer of size at most CN\_MERGE\_BUFFER\_R. Two memory accesses per element are required for the merge.
	\item Compressing (and splitting) the sorted in-memory buffer (once it is full) into compressed sorted sequences (chunks) to send to the assigned CDN.
	\item Sending all the chunks to assigned CDN.
\end{itemize}
\begin{displaymath}
T_{receive} = \frac{SDN_{gap}^{read}}{\texttt{NUM\_SDN}}
\end{displaymath}
\begin{displaymath} %\notag
T_{decomp} = \frac{\texttt{CN\_RECV\_BUFFER}}{\texttt{DECOMPRESSION\_RATE}}
\end{displaymath}
\begin{displaymath}
T_{merge} = 2 \times \frac{\texttt{CN\_MERGE\_BUFFER\_R}}{\texttt{MEMORY\_BW}}
\end{displaymath}
\begin{displaymath}
T_{comp} = \frac{\texttt{CN\_MERGE\_BUFFER\_R}}{\texttt{COMPRESSION\_BW} \times \texttt{CORES\_PER\_NODE}}
\end{displaymath}
\begin{displaymath}
T_{send} = \frac{\texttt{CN\_MERGE\_BUFFER\_R} \times \texttt{WCCR}}{\texttt{NETWORK\_BW}}
\end{displaymath}

Due to the double buffering, $T_{receive}$ is overlapped with $T_{decomp}$ followed by $T_{merge}$. Once the in-memory buffer is full (after $\texttt{NUM\_CHUNKS}$ round of receiving, decompressing, and merging), the compression of the in-memory buffer happens. All of these operations are overlapped with $T_{send}$. Therefore, the time it takes for a CN from the moment it starts receiving data to the moment it completes preparing and sending data to a CDN (which we will call CN's read gap), can be expressed using the following formula.
\begin{dmath*}
CN_{gap}^{read} = max\left\{ \texttt{NUM\_CHUNKS}\times
max\left\{ T_{receive}, T_{decomp} + T_{merge} \right\} + T_{comp}, T_{send} \right\}
\end{dmath*}

The total effective execution time of a CN in the read phase is the sum of all these gaps for all the series. This can be expressed as:
\begin{dmath*}
	T_{CN}^{read}=\texttt{NUM\_SERIES} \times CN_{gap}^{read}
\end{dmath*}


\subsubsection{Compute-side Data Nodes: Read Phase}
There are two major operations for \textbf{CDNs} during read phase:
\begin{itemize}
	\item Receiving a compressed chunk sent by a CN. The size of this message is at most CHUNK\_SIZE $\times$ WCCR. To estimate the time of this operation, we need to account for the time that the CNs need to compute and send these chunks, i.e. the $CN_{gap}^{read}$ as defined earlier. Since a CDN is expected to receive messages from all the CNs that are assigned to it during this phase and that each CN sends roughly NUM\_CHUNKS chunks, we can assume that the effective delay due to the $CN_{gap}^{read}$ is estimated by $\frac{CN_{gap}^{read} \times \texttt{NUM\_CDN}}{\texttt{NUM\_CHUNKS} \times \texttt{NUM\_CN}}$. This estimate implicitly includes the time it takes to transfer the message through the network.
	\item Writing the received message to the local disk.
\end{itemize}
\begin{displaymath}
T_{receive} = \frac{CN_{gap}^{read} \times \texttt{NUM\_CDN}}{\texttt{NUM\_CHUNKS} \times \texttt{NUM\_CN}}
\end{displaymath}
\begin{displaymath}
T_{write} = \texttt{SSD\_LATENCY} + \frac{\texttt{CHUNK\_SIZE} \times \texttt{WCCR}}{\texttt{SSD\_BW}}
\end{displaymath}

Due to the double buffering, $T_{receive}$ is overlapped with $T_{write}$, thus the time between writing consecutive received chunks to the local disk can be estimated as:
\begin{dmath*}
CDN_{gap}^{read} = max \left\{ T_{receive}, T_{write} \right\}
\end{dmath*}
The total effective execution time of a CDN in the read phase is the sum of these gaps for all chunks of all series coming from CNs that send messages to that CDN. This can be expressed as:
\begin{dmath*}
	T_{CDN}^{read}=\texttt{NUM\_CHUNKS} \times \texttt{NUM\_SERIES} \times \frac{\texttt{NUM\_CN}}{\texttt{NUM\_CDN}} \times CDN_{gap}^{read}
\end{dmath*}

\subsubsection{Compute-side Data Nodes: Write Phase}
Two major operations of \textbf{CDNs} during write phase are as follows:
\begin{itemize}
	\item Reading a compressed chunk from the local disk.
	\item Sending the read compressed chunk to a CN.
\end{itemize}

\begin{displaymath}
T_{read} = \texttt{SSD\_LATENCY} +
\frac{\texttt{CHUNK\_SIZE} \times \texttt{WCCR}}{\texttt{SSD\_BW}}
\end{displaymath}
\begin{displaymath}
T_{send} = \frac{\texttt{CHUNK\_SIZE} \times \texttt{WCCR}}{\texttt{NETWORK\_BW}}
\end{displaymath}

Due to the double buffering, $T_{read}$ is overlapped with $T_{send}$. Additionally, each CDN is expected to send roughly $\frac{NUM\_CN}{NUM\_CDN} \times NUM\_SERIES \times NUM\_CHUNKS$ chunks, so it is safe to assume that a CDN will always be busy reading chunks from local disk. Therefore, the time between sending two consecutive read compressed chunks can be estimated as:
\begin{displaymath}
CDN_{gap}^{write} = max \left\{ T_{read}, T_{send} \right\}
\end{displaymath}

The effective total time of a CDN in the write phase is calculated similar to the read phase, and can be expressed as follows:
\begin{dmath*}
	T_{CDN}^{write}=\texttt{NUM\_CHUNKS} \times \texttt{NUM\_SERIES} \times \frac{\texttt{NUM\_CN}}{\texttt{NUM\_CDN}} \times CDN_{gap}^{write}
\end{dmath*}


\subsubsection{Compute Nodes: Write Phase}
There are five major operations done by \textbf{CNs} during write phase:
\begin{itemize}
	\item Receiving enough data/chunks (from CDNs) that would fill the in-memory merge buffer. To estimate the time of this operation, we need to account for the time a CDN needs to read and send these chunks, i.e. the $CDN_{gap}^{read}$ as defined earlier. Since a CDN is expected to send one message per $CDN_{gap}^{read}$ time, a CN would wait for $\frac{NUM\_CN}{NUM\_CDN} \times CDN_{gap}^{read}$ to receive a compressed chunk from its assigned CDN. Hence it takes $\frac{NUM\_CN}{NUM\_CDN} \times \frac{CN\_MERGE\_BUFFER\_W}{CHUNK\_SIZE} \times CDN_{gap}^{write}$ to receive all data to fill the in-memory merge buffer. This estimate implicitly includes the time for message transfer through the network.
	\item Decompressing the received chunks.
	\item Merging received data into the in-memory buffer.
	\item Compressing the in-memory buffer once the buffer is full.
	\item Sending the compressed in-memory buffer to SDNs to write the final result back to parallel file system.
\end{itemize}

\begin{displaymath}
T_{receive} =  \frac{\texttt{NUM\_CN}}{\texttt{NUM\_CDN}} \times \frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{CHUNK\_SIZE}} \times CDN_{gap}^{write}
\end{displaymath}
\begin{displaymath}
T_{decomp} = \frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{DECOMPRESSION\_BW}}
\end{displaymath}
\begin{displaymath}
T_{merge} = 2 \times\frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{STREAM\_BW}}
\end{displaymath}
\begin{displaymath}
T_{comp} = \frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{COMPRESSION\_BW}}
\end{displaymath}
\begin{displaymath}
T_{send} = \frac{\texttt{CN\_MERGE\_BUFFER\_W} \times \texttt{WCCR}}{\texttt{NETWORK\_BW}}
\end{displaymath}

Based on the algorithm, message receiving, message sending, and decompression-merging-compression are overlapped. Therefore, the time between sending two consecutive compressed merge buffers can be estimated by the following formula:
\begin{displaymath}
CN_{gap}^{write} = max \left\{ T_{receive},
T_{decomp} + T_{merge} + T_{comp}, T_{send} \right\}
\end{displaymath}

The total effective execution time of a CN in the write phase is the sum of these gaps for all the communication rounds where the CN sends data to SDNs. Assuming that CNs handle almost equal share of input data, the total effective execution time of each CN can be expressed as:

 \begin{dmath*}
 	T_{CN}^{write}=\frac{\texttt{FILE\_SIZE}}{\texttt{NUM\_CN} \times \texttt{CN\_MERGE\_BUFFER\_W}} \times CN_{gap}^{write}
 \end{dmath*}
 
\subsubsection{Storage-side Data Nodes: Write Phase}
Major operations of \textbf{SDNs} during write phase are as follows:
\begin{itemize}
	\item Receiving a compressed sorted buffer from a CN. To estimate the time of this operation, we need to account for the time that a CN needs to compute and send such buffer, i.e. the $CN_{gap}^{write}$ as defined earlier. Since $\frac{NUM\_CN}{NUM\_SDN}$ CNs are assigned to a single SDN, a SDN would wait for $\frac{NUM\_SDN}{NUM\_CN} \times CN_{gap}^{write}$ to receive a compressed merge buffer.
	\item Decompressing the received sorted buffer.
	\item Writing the decompressed sorted buffer to the parallel file system.
\end{itemize}

\begin{displaymath}
T_{receive} =  \frac{\texttt{NUM\_SDN}}{\texttt{NUM\_CN}} \times CN_{gap}^{write}
\end{displaymath}
\begin{displaymath}
T_{decomp} = \frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{DECOMPRESSION\_BW}}
\end{displaymath}
\begin{displaymath}
T_{write} = \texttt{PFS\_LATENCY} +
\frac{\texttt{CN\_MERGE\_BUFFER\_W}}{\texttt{PFS\_BW}}
\end{displaymath}

Due to the double buffering, message receiving and decompressing is overlapped with file writing. The time between writing two consecutive sorted buffers into the parallel file system can be estimated as:
\begin{displaymath}
SDN_{gap}^{write} = max \left\{ T_{receive} + T_{decomp}, T_{write} \right\}
\end{displaymath}

Assuming that SDNs handle almost equal share of the entire data, we can express the total effective execution time of each SDN as follows:
\begin{dmath*}
	T_{SDN}^{write}=\frac{\texttt{FILE\_SIZE}}{\texttt{NUM\_SDN} \times \texttt{CN\_MERGE\_BUFFER\_W}} \times SDN_{gap}^{write}
\end{dmath*}

\subsubsection{Putting It All Together}
Since different components of the DEP architecture can work in parallel, we can express the total execution time of the algorithm as follows:
\begin{dmath*}
T=max\left\{T_{SDN}^{read},T_{CDN}^{read},T_{CN}^{read}\right\}+max\left\{T_{SDN}^{write},T_{CDN}^{write},T_{CN}^{write}\right\}
\end{dmath*}