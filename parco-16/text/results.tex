\section{Evaluation}
\label{evaluation}
In this section we describe the characteristics of the testbed system of our choice, we provide an overview of microbenchmarks chosen to find critical system parameters, and finally we demonstrate tuning results of our implementation and performance comparison with BigSort algorithm (Integer sorting in CORAL benchmark~\cite{coral}).

\subsection{System Setup}
Our experiments were conducted on a 65-node SUN Fire Linux-based cluster, with one head node and 64 computing nodes. Each compute node was equipped with a quad-core AMD Opteron that supports up to 8 simultaneous integer threads. A selected number of nodes were also equipped with SSD along with HDD. Figure~\ref{fig:hectopo} depicts the network topology of this cluster. We used the MPICH 3.1.4 release and installed a PVFS2 parallel file system~\cite{pvfs}. The file system we deployed was OrangeFS version 2.8.2.

\begin{wrapfigure}{R}{0.45\textwidth}
    \centering
    \includegraphics[width=0.4\textwidth]{figures/hectopo.png}
    \caption{Topology of the experiment cluster.}
    \label{fig:hectopo}
\end{wrapfigure}

The DEP system architecture assumes the close proximity of Storage-side Data Nodes (SDNs) to file-servers and also Compute-side Data Nodes (CDNs) to Compute Nodes (CNs). We chose the above testbed machine for several reasons, mostly because it provides us with certain capabilities: 1) all nodes are equipped with hard drives and some with SSDs making it easier to set specific nodes to represent different group of nodes (i.e. SDNs and CDNs), 2) the network of the cluster also allows us to mimic the DEP network topology such as high bandwidth between SDNs and file-servers or CDNs and CNs, and 3) much like current supercomputers, we are able to see the effect of the limited bandwidth between CNs and SDNs by selecting appropriate subsets of nodes.

Specifically, our experiment setup was as follows. We deployed a PVFS2 instance on 8 nodes on switch 0 using their HDD as the storage layer. We chose SDNs amongst available nodes of switch 0 exploiting the high intra-node bandwidth to the parallel file system, whereas on the other hand, we chose CDNs and CNs amongst nodes on switches 1 and 2 using the slower inter-node link.

In the end-to-end comparison we compared our sorting algorithm with BigSort. We used the same PFVS2 configuration for BigSort. However, we chose compute nodes running BigSort amongst nodes outside of switch 0, where PVFS2 was deployed, making our comparison as fair as possible.

\subsection{Benchmarking and System Parameters}
\START
We ran a collection of micro-benchmarks to discover the values of the system parameters to better tune our implementation:
\begin{itemize}
  \item \textbf{Latency of parallel file system.} This is achieved by measuring the time it takes to write/read a small portion to/from a random location of a file. We executed thousands of accesses and took the average.
  \item \textbf{Bandwidth to parallel file system.} This is achieved by running a
  micro-benchmark that uses MPI-IO collective operations to read/write a contiguous
  linear array with 128 MiB of data from/to the parallel file system. Note that if we run the benchmark on nodes within the same switch as PVFS2, we get 162 MiB/s bandwidth, while if we run the benchmark from another switch the bandwidth is 111 MiB/s. In the latter case limited network bandwidth between two switches causes a reduction of the effective I/O throughput seen by the compute nodes. The result of this benchmark signifies even more the benefits of using SDNs in the DEP architecture. In general, it is expected that the SDNs get a higher bandwidth to parallel file system due to their proximity to file-servers.
  \item \textbf{Latency and bandwidth to local SSD.} Similarly, we ran the same micro-benchmarks but this time accessing the local SSD instead of the parallel file system.
  \item \textbf{Memory bandwidth.} To measure the main memory bandwidth of our testbed machine we ran STREAM~\cite{stream} benchmark.
  \item \textbf{Compression and decompression bandwidth.} We ran the compression algorithm on 128 MiB of data (integers) to measure this parameter.
\end{itemize}
Table~\ref{table:systemmodelparams} contains the full list of the values of system parameters obtained by running these benchmarks.

\begin{table}[t]
\caption{Tunable and Derived Model Parameters}
\centering
\tiny
\begin{tabular}{l l}
\hline \hline
Parameter & Value \\ [0.5ex]
\hline
NUM\_SDN             &  1,2,4\\
NUM\_CDN             &  1,2,4\\
NUM\_CN             &  4,8,12,16,20,24\\
CORES\_PER\_NODE    &  8\\
DRAM\_ALLOCATION    &  1 GiB\\
SDN\_BUFFER\_SIZE\tablefootnote{Size of in-memory buffer} &  32,64,128 MiB\\
SDN\_READ\_SIZE     &  16,32 MiB\\

CN\_MERGE\_BUFFER\_R\tablefootnote{Merge buffe size in read phase}   &  200 MiB\\
CHUNK\_SIZE        &  16,20 MiB\\
CN\_MERGE\_BUFFER\_W\tablefootnote{Merge buffe size in write phase} &  SDN\_BUFFER\_SIZE\\
TYPE\_SIZE          &  4 bytes (integers)\\
FILE\_SIZE          &  \scriptsize{$2\times(NUM\_SDN+NUM\_CDN+$}\\
~  & \scriptsize{\ \ \ \ \ \ $NUM\_CN)$} GiB\\
CN\_RECV\_BUFFER &  \scriptsize{$\frac{SDN\_BUFFER\_SIZE}{NUM\_CN}$}\\
NUM\_CHUNKS      &  \scriptsize{$\frac{CDN\_MERGE\_BUFFER}{CHUNK\_SIZE}$}\\
NUM\_SERIES      &  \scriptsize{$\frac{FILE\_SIZE}{NUM\_CN \times CN\_MERGE\_BUFFER\_R}$}\\ [1ex]
\hline
\end{tabular}
\label{table:tunablemodelparams}
\end{table}

\begin{table}[t]
\caption{System Model Parameters}
\centering
\tiny
\begin{tabular}{l l}
\hline \hline
Parameter & Value \\ [0.5ex]
\hline
PFS\_LATENCY        &  2.8 ms\\
PFS\_BW             &  111/162 MiB/s\\
SSD\_LATENCY        &  5 us\\
SSD\_BW             &  368 MiB/s\\
MEMORY\_BW          &  2.8 GiB/s\\
NETWORK\_BW         &  10.9 MiB/s\\
COMPRESSION\_BW   & 520 MiB/s \\
DECOMPRESSION\_BW & 604 MiB/s \\
WCCR\tablefootnote{Worst-case compression ratio} &  0.3-0.5\\ [1ex]
\hline
\end{tabular}
\label{table:systemmodelparams}
\end{table}
 
\subsection{Performance Tuning}
\START
There are opportunities for performance optimization by tuning two parameters: the size of the various buffers used and the ratio of number of CNs to CDNs and SDNs. We used the performance model to get the optimal size of the buffers used in different types of nodes. This tuning considers the memory requirement of each type of node and the restriction that the total amount of memory usage in each node should not exceed the total available RAM (i.e. DRAM\_ALLOCATION). 
Table~\ref{table:tunablemodelparams} contains values of tunable and derived parameters obtained from performance model.



Figure~\ref{fig:tuning} shows the effect of varying number of CDNs/SDNs in performance. It is clear that with constant number of CNs, larger number of CDNs and/or SDNs leads to lower total execution time. In the extreme case where the number of CNs, CDNs, and SDNs are all equal, each SDN/CDN is responsible to serve exclusively one CN. This causes the minimum amount of contention on CDNs/SNDs, and maximizes the parallelization. However, in larger-scale experiments, CNs are the most numerous resources available in a system, hence matching up the number of CDNs and SDNs to number of CNs is not usually feasible. Therefore, for the rest of our experiments on our small-scale prototype testbed, we assume that the number of SDNs and also the number of CDNs is four.

Note that the times derived from performance model are very close in almost all cases to actual execution times in different settings in Figure~\ref{fig:tuning}. This means we can simply use the performance model to predict the optimal number of CNs, CDNs, and SDNs in a different DEP cluster/setting and avoid running the algorithm (which may take rather long) for the sake of tuning.

\begin{figure}[t]
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/iof_effect}
  \caption{Effect of number of CDNs (with 4 CN, 4 SDN)}
  \label{fig:iof_effect}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/fs_effect}
  \caption{Effect of number of SDN (with 4 CN, 4 CDN)}
  \label{fig:fs_effect}
\end{subfigure}
\caption{Effect of different number of CDNs and SDNs on performance. File size in all these experiments was 16 GiB.}
\label{fig:tuning}
\end{figure}

\subsection{Scalability Study}
\START

\begin{figure*}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/sca_time}
  \caption{Sort Execution Time}
  \label{fig:sca_time}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/sca_rate}
  \caption{Sort Rate}
  \label{fig:sca_rate}
\end{subfigure}
\caption{Weak scaling study of DEPSort and BigSort.}
\label{fig:sca}
\end{figure*}

We compare our implementation with BigSort and Figure~\ref{fig:sca} depicts the weak scalability graph for execution time~(\ref{fig:sca_time}) and sorting rate~(\ref{fig:sca_rate}). For each experiment, the file size is twice the aggregate memory allocation of the total number of nodes (i.e. for 12 nodes we used 24 GiB file size, for 16 nodes we used 32 GiB file size, and so on). To have a fair comparison, the total number of nodes reported in the graph for DEPSort is equal to the sum of number of CNs, CDNs, and SDNs. As mentioned before, we use 4 CDNs and 4 SDNs for DEPSort experiments. DRAM\_ALLOCATION for both DEPSort and BigSort is set to 1 GiB.

First, the performance model matches almost perfectly with the timing from the actual experiments on the cluster. This means the performance model is accurate enough and can be used to predict the execution time of our sorting algorithm on a DEP cluster.

Second, the average rate at which our algorithm sorts the data is 71 MiB/s. Considering that bandwidth to PVFS2 from SDNs is 162 MiB/s, and also the fact that for sorting the data we have to at least read the entire raw data once and write the final sorted data back to the disk, the best achievable rate is 81 MiB/s. This means our sorting technique almost entirely overlaps computation and communication with just reading the raw data and writing back the sorted data. Note that the maximum achievable sort rate with a non-DEP implementation on the same cluster is 55 MiB/s (due to the 111 MiB/s bandwidth from CNs to PFVS2 mentioned in Table~\ref{table:systemmodelparams}). This proves the effectiveness of DEP architecture where having SDNs closer to parallel file system improves the performance by at least 30\% compared to the best possible sort algorithm in a non-DEP architecture.

Third, DEPSort is on average 4.4X faster than BigSort. This is due to the entire overlap of communication and computation with disk operation in DEPSort, and also the fact that DEPSort compresses the intermediate results and stores them in fast SSDs. In case of BigSort, the intermediate results are written back to the parallel file system, and also the actual sorting algorithm reads and writes the intermediate data multiple times. The benefit of our algorithm in performance comes from two different reasons: 1) the algorithmic changes we proposed (overlapping computation with communication, compressing the intermediate results), and 2) from the DEP architecture itself (higher I/O throughput through SDNs and fast SSD devices close to CNs).

Lastly, as expected, the performance of DEPSort gradually drops at larger number of nodes. This is mainly because of the contention over CDNs and SDNs. In the largest case, we have 20 CNs, 4 CDNs, and 4 SDNs. That is a ratio of 5 for the number of CNs over 1 CDN/SDN. Due to the resource limitations in our prototype DEP cluster we were not able to run the algorithm at larger scale and discover what the best ratio of CNs over CDNs/SDNs is for which the performance is still acceptable.
