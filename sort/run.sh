#!/bin/bash

for i in {1..100}
do
  echo "round $i"
  rm sorted.dat; mpiexec -n 5 ./sort >output 2>&1; ./extract.sh
done
