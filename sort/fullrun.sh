#!/bin/bash

# pass number of MPI processes and array size (elements)
# as command line argument
p=$1
elems=$2
bytes=$(expr 4 \* $2)
num_rounds=$3

for((i=1; i<=${num_rounds}; i++)); do
  echo "round $i"
  rm -f sorted.dat
  mpiexec -n $1 ./sort >output 2>&1
  if [ `du -b sorted.dat | awk '{ print $1 }'` != ${bytes} ]; then
    echo "Error: unexpected file size!"
    exit 1
  fi
  ../data/checker sorted.dat $elems > /dev/null 
  if [ $? -eq 1 ]; then
    echo "Error: unsorted file!"
    exit 1
  fi
done
