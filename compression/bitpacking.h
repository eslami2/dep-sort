void ipackwithoutmask0(__m128i, const uint32_t *, __m128i *) {}

void ipackwithoutmask1(__m128i initOffset, const uint64_t *_in, __m128i *out) {
  const __m128i *in = reinterpret_cast<const __m128i *>(_in);
  __m128i OutReg;

  __m128i CurrIn = _mm_load_si128(in);
  __m128i InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;
  OutReg = InReg;
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 1));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 2));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 3));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 4));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 5));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 6));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 7));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 8));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 9));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 10));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 11));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 12));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 13));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 14));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 15));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 16));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 17));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 18));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 19));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 20));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 21));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 22));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 23));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 24));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 25));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 26));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 27));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 28));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 29));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 30));
  ++in;
  CurrIn = _mm_load_si128(in);
  InReg = Delta(CurrIn, initOffset);
  initOffset = CurrIn;

  OutReg = _mm_or_si128(OutReg, _mm_slli_epi32(InReg, 31));
  _mm_store_si128(out, OutReg);
}

typedef void (*integratedpackingfunction)(__m128i, const uint64_t *, __m128i *);

integratedpackingfunction packwithoutmask[33] = {
    ipackwithoutmask0,  ipackwithoutmask1,  ipackwithoutmask2,
    ipackwithoutmask3,  ipackwithoutmask4,  ipackwithoutmask5,
    ipackwithoutmask6,  ipackwithoutmask7,  ipackwithoutmask8,
    ipackwithoutmask9,  ipackwithoutmask10, ipackwithoutmask11,
    ipackwithoutmask12, ipackwithoutmask13, ipackwithoutmask14,
    ipackwithoutmask15, ipackwithoutmask16, ipackwithoutmask17,
    ipackwithoutmask18, ipackwithoutmask19, ipackwithoutmask20,
    ipackwithoutmask21, ipackwithoutmask22, ipackwithoutmask23,
    ipackwithoutmask24, ipackwithoutmask25, ipackwithoutmask26,
    ipackwithoutmask27, ipackwithoutmask28, ipackwithoutmask29,
    ipackwithoutmask30, ipackwithoutmask31, ipackwithoutmask32 /* TODO */};

static inline void SIMDipackwithoutmask(__m128i initOffset, const uint64_t *in,
                                        __m128i *out, const uint64_t bit) {
  packwithoutmask[bit](initOffset, in, out);
}

