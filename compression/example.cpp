//
// A simple example to get you started with the library.
// You can compile and run this example like so:
//
//   make example
//   ./example
//
//  Warning: If your compiler does not fully support C++11, some of
//  this example may require changes.
//

#include <vector>
#include <algorithm>
#include <iostream>
#include <sys/time.h>



const size_t SIMDBlockSize = 128;
const uint32_t CookiePadder = 123456;  // just some made up number
const uint32_t MiniBlockSize = 128;
const uint32_t HowManyMiniBlocks = 8;
const uint32_t BlockSize = MiniBlockSize;  // HowManyMiniBlocks * MiniBlockSize

template <class T>
__attribute__((const)) inline bool needPaddingTo128Bits(const T *inbyte) {
  return reinterpret_cast<uintptr_t>(inbyte) & 15;
}

__attribute__((always_inline))
inline __m128i Delta(__m128i curr, __m128i prev) {
  return _mm_sub_epi64(
      curr, _mm_or_si128(_mm_slli_si128(prev, 8), _mm_srli_si128(curr, 8)));
}

__attribute__((const))
inline uint64_t gccbits(const uint64_t v) {
    return v == 0 ? 0 : 64 - __builtin_clzll(v);
}

// this macro should not be required if true C++11 could be counted on
#if defined(__alignas_is_defined)
#define ALIGN16 alignas(16)
#elif defined(__GNUC__)
#define ALIGN16 __attribute__((aligned(16)))
#else
#define ALIGN16 __declspec(align(16))
#endif

/**
 * Treats  __m128i as 2 x 64-bit integers and asks for the max
 * number of bits used (integer logarithm).
 */
inline uint64_t maxbitas64int(const __m128i accumulator) {
    ALIGN16 uint64_t tmparray[2];
    _mm_store_si128(reinterpret_cast<__m128i *>(tmparray), accumulator);
    return gccbits(tmparray[0] | tmparray[1]);
}

static uint64_t maxbits(const uint64_t *in, __m128i &initoffset) {
  const __m128i *pin = reinterpret_cast<const __m128i *>(in);
  __m128i newvec = _mm_load_si128(pin);
  __m128i accumulator = Delta(newvec, initoffset);
  __m128i oldvec = newvec;
  for (uint32_t k = 1; 2 * k < SIMDBlockSize; ++k) {
    newvec = _mm_load_si128(pin + k);
    accumulator = _mm_or_si128(accumulator, Delta(newvec, oldvec));
    oldvec = newvec;
  }
  initoffset = oldvec;
  return maxbitas64int(accumulator);
}

static void packblockwithoutmask(uint64_t *in, uint64_t *out,
                                 const uint64_t bit, __m128i &initoffset) {
  __m128i nextoffset =
      _mm_load_si128(reinterpret_cast<__m128i *>(in + SIMDBlockSize - 2));
  SIMDipackwithoutmask(initoffset, in, reinterpret_cast<__m128i *>(out), bit);
  // TODO
  initoffset = nextoffset;
}

void SIMDBinaryPacking_encodeArray(uint64_t *in, const size_t length,
                                   uint64_t *out, size_t &nvalue) {
  const uint64_t *const initout(out);
  if (needPaddingTo128Bits(out) or needPaddingTo128Bits(in))
    throw std::runtime_error(
        "alignment issue: pointers should be aligned on 128-bit boundaries");
  *out++ = static_cast<uint64_t>(length);
  while (needPaddingTo128Bits(out)) *out++ = CookiePadder;
  uint64_t Bs[HowManyMiniBlocks];
  __m128i init = _mm_set1_epi64(0);
  const uint64_t *const final = in + length;
  for (; in + HowManyMiniBlocks * MiniBlockSize <= final;
       in += HowManyMiniBlocks * MiniBlockSize) {
    __m128i tmpinit = init;
    for (uint32_t i = 0; i < HowManyMiniBlocks; ++i) {
      Bs[i] = maxbits(in + i * MiniBlockSize, tmpinit);
    }
    *out++ = (Bs[0] << 56) | (Bs[1] << 48) | (Bs[2] << 40) | (Bs[3] << 32) |
             (Bs[4] << 24) | (Bs[5] << 16) | (Bs[6] << 8) | Bs[7];
    for (uint32_t i = 0; i < HowManyMiniBlocks; ++i) {
      packblockwithoutmask(in + i * MiniBlockSize, out, Bs[i], init);
      out += MiniBlockSize / 64 * Bs[i];
      // TODO
    }
  }
  if (in < final) {
    const size_t howmany = (final - in) / MiniBlockSize;
    __m128i tmpinit = init;
    memset(&Bs[0], 0, HowManyMiniBlocks * sizeof(uint32_t));
    for (uint32_t i = 0; i < howmany; ++i) {
      Bs[i] = BlockPacker::maxbits(in + i * MiniBlockSize, tmpinit);
    }
    *out++ = (Bs[0] << 24) | (Bs[1] << 48) | (Bs[2] << 40) | (Bs[3] << 32)
    | (Bs[4] << 24) | (Bs[5] << 16) | (Bs[6] << 8) | Bs[7];
    *out++ = (Bs[8] << 24) | (Bs[9] << 16) | (Bs[10] << 8) | Bs[11];
    *out++ = (Bs[12] << 24) | (Bs[13] << 16) | (Bs[14] << 8) | Bs[15];
    for (uint32_t i = 0; i < howmany; ++i) {
      BlockPacker::packblockwithoutmask(in + i * MiniBlockSize, out, Bs[i],
                                        init);
      out += MiniBlockSize / 32 * Bs[i];
    }
    in += howmany * MiniBlockSize;
    assert(in == final);
  }
  nvalue = out - initout;
}

void encodeArray(uint64_t *in, const size_t length, uint64_t *out,
                 size_t &nvalue) {
  const size_t roundedlength = length / BlockSize * BlockSize;
  size_t nvalue1 = nvalue;
  SIMDBinaryPacking_encodeArray(in, roundedlength, out, nvalue1);

  // TODO: continue from here
  if (roundedlength < length) {
    ASSERT(nvalue >= nvalue1, nvalue << " " << nvalue1);
    size_t nvalue2 = nvalue - nvalue1;
    codec2.encodeArray(in + roundedlength, length - roundedlength,
                       out + nvalue1, nvalue2);
    nvalue = nvalue1 + nvalue2;
  } else {
    nvalue = nvalue1;
  }
}

void dummy(uint32_t* buff, size_t buff_size);

using namespace SIMDCompressionLib;

double get_time() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 1.0e-6 * tv.tv_usec;
}

int main(int argc, char** argv) {
  // We pick a CODEC
  /*
ibp32
Compression Time (s): 0.024615
Compression Rate (MB/s): 5452.67
You are using 8.70066 bits per integer. 
Decompression Time (s): 0.028538
Decompression Rate (MB/s): 4703.13

s4-bp128-d1
Compression Time (s): 0.0212791
Compression Rate (MB/s): 6307.49
You are using 9.01272 bits per integer. 
Decompression Time (s): 0.0284009
Decompression Rate (MB/s): 4725.83

s4-bp128-d2
Compression Time (s): 0.021565
Compression Rate (MB/s): 6223.88
You are using 9.32712 bits per integer. 
Decompression Time (s): 0.0286229
Decompression Rate (MB/s): 4689.18

s4-bp128-dm
Compression Time (s): 0.021507
Compression Rate (MB/s): 6240.65
You are using 9.8178 bits per integer. 
Decompression Time (s): 0.028826
Decompression Rate (MB/s): 4656.13

*/   
//  IntegerCODEC &codec = *CODECFactory::getFromName("s4-bp128-dm");
  IntegerCODEC &codec = *CODECFactory::getFromName("ibp32");
  // could use others, e.g., "varint", "s-fastpfor-1"
  ////////////
  //
  // create a container with some integers in it
  //
  // We need the integers to be in sorted order.
  //
  // (Note: You don't need to use a vector.)
  //
  size_t N = 10 * 1000;
  if (argc > 1) {
    N = atoi(argv[1]);  
  }
  vector<uint32_t> mydata(N);
  for (int i = 0; i < N; ++i) {
    mydata[i] = rand();
  }

  cout << "Start sorting..." << endl;
  sort(mydata.begin(), mydata.end());
  cout << "Sorting done...." << endl;
  ///////////
  //
  // You need some "output" container. You are responsible
  // for allocating enough memory.
  //
  vector<uint32_t> compressed_output(N + 1024);
  // N+1024 should be plenty
  //
  //
  size_t compressedsize = compressed_output.size();
  double min_time = 1e6;
  for (int T= 0; T < 10; ++T) {
  double t = get_time();
  codec.encodeArray(mydata.data(), mydata.size(), compressed_output.data(),
                    compressedsize);
  t = get_time() - t;
  dummy(compressed_output.data(), compressedsize);
  if (t < min_time)
    min_time = t;
  }
  cout << "Compression Time (s): " << min_time << endl;
  cout << "Compression Rate (MB/s): " << N * 4 / (min_time * 1e6) << endl;
  //
  // if desired, shrink back the array:
  compressed_output.resize(compressedsize);
  compressed_output.shrink_to_fit();
  // display compression rate:
//  cout << setprecision(3);
  cout << "You are using "
       << 32.0 * static_cast<double>(compressed_output.size()) /
              static_cast<double>(mydata.size()) << " bits per integer. "
       << endl;
  //
  // You are done!... with the compression...
  //
  ///
  // decompressing is also easy:
  //
  vector<uint32_t> mydataback(N);
  size_t recoveredsize = mydataback.size();
  //
  min_time = 1e6;
  for (int T = 0; T<10; ++T) {
  double t = get_time();
  codec.decodeArray(compressed_output.data(), compressed_output.size(),
                    mydataback.data(), recoveredsize);
  t = get_time() - t;
  dummy(compressed_output.data(), compressedsize);
  if (t < min_time)
    min_time = t;
  }
  cout << "Decompression Time (s): " << min_time << endl;
  cout << "Decompression Rate (MB/s): " << N * 4 / (min_time * 1e6) << endl;
  mydataback.resize(recoveredsize);
  //
  // That's it for compression!
  //
  if (mydataback != mydata) throw runtime_error("bug!");
  return 0;
}
