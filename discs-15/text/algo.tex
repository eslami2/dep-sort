\section{Disk-to-Disk Sorting Algorithm}
\label{sec:algo}
\START
Our design of disk-to disk sorting was driven by the need to utilize all the different parts of the DEP architecture. SDNs are connected to the file system, therefore will be responsible for reading the input, distributing it appropriately to the remaining nodes in the system, and writing the final result back to the file system. CDNs, being connected to fast local SSDs, will be responsible for storing intermediate results. Finally, the CNs, being the most numerous type of nodes in the DEP architecture, will perform the main part of the computation.

The disk-to-disk sorting algorithm is divided into two phases, the read phase and the write phase. During the read phase, input data is read by the SDNs and distributed to the CNs, which perform some initial processing and send the data for intermediate storage to the CDNs. The end of the input data identifies the end of the read phase and the beginning of the write phase. During the write phase, CDNs send the intermediate results to CNs in an appropriate order. The CNs then compute the final result and send the sorted data to SDNs for storage.

The algorithm is capable of handling skewed data distribution using similar sampling technique discussed in~\cite{datasort}. Data sampling can be done amongst SDNs right before the first time they communicate with CNs. However, for the sake of simplicity, the algorithm we describe here (and also the analysis in Section~\ref{sec:model}) assumes that the input data follows a uniform distribution with known minimum and maximum values. Hence, we assume that throughout the entire sorting process, each CN deals only with a specific predefined subrange of the data. In order to achieve balance in the amount of data being processed by each CN, the subrange of the $i$-th CN is defined as $[\frac{i(max-min)}{NUM\_CN}, \frac{(i+1)(max-min)}{NUM\_CN} - 1]$. Also, since the CNs are the most numerous type of nodes, each SDN will eventually collect sorted data from a set of CNs with consequtive assigned subranges. The set of CNs assigned to each SDN is predefined, based on an internal ranking of the SDNs.

The following sections provide a more detailed description of operations done in different types of nodes during the read and write phases of the algorithm.

\subsection{Read Phase}
\label{sec:designRead}
\START
During the read phase (algorithms~\ref{alg:sdnread},~\ref{alg:cnread}, and~\ref{alg:cdnread}), the SDNs read data from the disk in batches, sort these batches, and send them to the appropriate CNs after compressing them. The SDNs use non-blocking MPI\-/IO read calls and double-buffering to efficiently read data through the storage device network. Each SDN overlaps reading the next data batch with processing of the current data batch. The processing involves sorting, partitioning, and compressing the data. First, the current data batch is sorted into sorted runs using local thread parallelism. The sorted runs in turn are merged with the already sorted data found in the SDN's memory (represented by the \texttt{Mem1} and \texttt{Mem2} buffers in the pseudocode). When the SDN's memory is full, the sorted data are partitioned according to the CN subranges and each partition is compressed and sent to the corresponing CN. Again, the sending time is overlapped with the combined IO, processing, and compressing time. Moreover, sending compressed sorted data allows for smaller messages as the compression ratio for sorted data is higher.

Each CN waits for messages of sorted data sent from SDNs. While waiting for the next message, it decompresses and merges the latest received data with data already found in its memory. When the CN's memory is full, it sends the data to its assigned CDN as a series of compressed chunks. The size of the chunk is a user defined constant and the function call $chunk(i)$ computes the range of the $i$-th chunk. Note that each series of chunks contains sorted data belonging to the CN's predefined range. The compressing plus sending opeartions are overlapped with the rest of the operations.

Finally, each CDN collects data from all its assigned CNs and stores them in a file on its local SSD. The file is indexed by CN rank, series number, and chunk number for easy extraction of the information. The read phase ends when all input data has been read by the SDNs, processed through the CNs, and stored in the form of sorted series of chunks in the SSDs of the CDNs.

\subsection{Write Phase}
\label{sec:designWrite}
\START
During the write phase (algorithms~\ref{alg:sdnwrite},~\ref{alg:cnwrite}, and~\ref{alg:cdnwrite}), each CDN serves requests from its assigned CNs for chunks from specific series. The requests come in the form of series numbers. The CDN keeps track of the last sent chunk of each series for each CN and always sends the next chunk in order.
\begin{algorithm}
\caption{Storage-side Data Node Read Phase}
\label{alg:sdnread}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{ReadBuf} $\gets$ \Call{read()}{}
%\State \Call{wait}{\texttt{ReadReq}}

\State \Comment Main Loop
\While{not $EOF$ found}
  \State \Call{swap}{\texttt{ReadBuf}, \texttt{DataBuf}}
  \State \texttt{ReadBuf} $\gets$ \Call{Iread}{\texttt{ReadReq}}
  \State \texttt{SortedRunsBuf} $\gets$ \Call{ParallelSort}{\texttt{DataBuf}}
  \State \texttt{SortedBuf} $\gets$ 
  \State \Call{\ \ \ \ \ \ MultiWayMerge}{\texttt{SortedRunsBuf}, \texttt{SortedBuf}}
  \If {\texttt{SortedBuf.lenght} + \texttt{Mem1.length} > \texttt{Mem2.size}}
    \State \Call{waitall}{\texttt{SendReqs}}
    \For{each Compute Node \texttt{CN}, in parallel}
      \State \texttt{SendBufs}[\texttt{CN}] $\gets$ \Call{compress}{\texttt{Mem1}[$range$(\texttt{CN})]}
      \State \Call{Isend}{\texttt{SendBufs}[\texttt{CN}], \texttt{CN}, \texttt{SendReqs}[\texttt{CN}]}
    \EndFor
    \State \texttt{Mem1.clear()}
  \EndIf
  \State \texttt{Mem2} $\gets$ \Call{merge}{\texttt{SortedBuf}, \texttt{Mem1}}
  \State \Call{swap}{\texttt{Mem1}, \texttt{Mem2}}
  \State \Call{wait}{\texttt{ReadReq}}
\EndWhile

\end{algorithmic}
}
\end{algorithm}


\begin{algorithm}
\caption{Compute Node Read Phase}
\label{alg:cnread}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{NumSeries} $\gets$ 1
\State \texttt{RecvBuf} $\gets$ \Call{recv}{\textit{FROM\_ANY\_SDN}}
%\State \Call{wait}{\texttt{RecvReq}, \&\texttt{SDN}}

\State \Comment Main Loop
\While{not received finisher}
  \State \Call{swap}{\texttt{RecvBuf}, \texttt{CompDataBuf}}
  \State \texttt{RecvBuf} $\gets$ \Call{Irecv}{\textit{FROM\_ANY\_SDN}, \texttt{RecvReq}}
  \State \texttt{DataBuf} $\gets$ \Call{decompress}{\texttt{CompDataBuf}}
  \If{\texttt{DataBuf.length} + \texttt{Mem1.length} > \texttt{Mem2.size}}
    \State \Call{waitall}{\texttt{SendReqs}}
    \State \texttt{NumChunks} $\gets$ \texttt{Mem1.length} / \textit{CHUNK\_SIZE}
    \For{\texttt{i} = 1 \textbf{to} \texttt{NumChunks}, in parallel}
      \State \texttt{SendBufs}[\texttt{i}] $\gets$ \Call{compress}{\texttt{Mem1}[$chunk$(\texttt{i})]}
    \EndFor
    \For{\texttt{i} = 1 \textbf{to} \texttt{NumChunks}}
      \State \texttt{msg} $\gets$ \{\texttt{NumSeries}, \texttt{i}, \texttt{SendBuffs}[\texttt{i}]\}
      \State \Call{Isend}{\texttt{msg}, \textit{TO\_MY\_CDN}, \texttt{SendReqs}[\texttt{i}]}
    \EndFor
    \State \texttt{Mem1.clear()}
    \State \texttt{NumSeries} $\gets$ \texttt{NumSeries} + 1
  \EndIf
  \State \texttt{Mem2} $\gets$ \Call{merge}{\texttt{DataBuf}, \texttt{Mem1}}
  \State \Call{swap}{\texttt{Mem1}, \texttt{Mem2}}
  \State \Call{wait}{\texttt{RecvReq}}
\EndWhile

\end{algorithmic}
}
\end{algorithm}

\begin{algorithm}
\caption{Compute-side Data Node Read Phase}
\label{alg:cdnread}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{finished} $\gets$ 0
\For{each compute node \texttt{CN} assigned to this \texttt{CDN}}
  \State \texttt{MsgBufs}[\texttt{CN}] $\gets$ \Call{Irecv}{from \texttt{CN}, \texttt{RecvReqs}[\texttt{CN}]}
\EndFor

\State \Comment Main Loop
\While{\texttt{finished} < \texttt{assigned}}
  \State \texttt{CN} $\gets$ \Call{waitany}{\texttt{RecvReqs}}
  \If{received finisher}
    \State \texttt{finished} $\gets$ \texttt{finished} + 1
    \State \textbf{continue}
  \EndIf
  \State \texttt{series} $\gets$ \texttt{MsgBufs}[\texttt{CN}]\texttt{.series}
  \State \texttt{chunkNo} $\gets$ \texttt{MsgBufs}[\texttt{CN}]\texttt{.chunkNo}
  \State \Call{swap}{\texttt{MsgBufs}[\texttt{CN}]\texttt{.chunkBuf}, \texttt{ToFileBuf}}
  \State \texttt{MsgBufs}[\texttt{CN}] $\gets$ \Call{Irecv}{\texttt{CN}, \texttt{RecvReqs}[\texttt{CN}]}
  \State \Call{WriteToLocalFile}{\texttt{CN}, \texttt{series}, \texttt{chunkNo}, \texttt{ToFileBuf}}
\EndWhile

\end{algorithmic}
}
\end{algorithm}

\begin{algorithm}
\caption{Storage-side Data Node Write Phase}
\label{alg:sdnwrite}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{finished} $\gets$ 0
\State \texttt{RecvBuf} $\gets$ \Call{Irecv}{$FROM\_ANY\_CN$}
%\State \Call{wait}{\texttt{RecvdReq}, \&\texttt{CN}}

\State \Comment Main Loop
\While{\texttt{finished} < \texttt{assigned}}
  \State \texttt{CN} $\gets$ \texttt{RecvBuf.source}
  \State \Call{swap}{\texttt{RecvBuf}, \texttt{CompDataBuf}}
  \State \texttt{RecvBuf} $\gets$ \Call{Irecv}{$FROM\_ANY\_CN$, \texttt{RecvReq}}
  \If {received finisher}
    \State \texttt{finished} $\gets$ \texttt{finished} + 1
    \State \textbf{continue}
  \EndIf
  \State \Call{wait}{\texttt{WriteReq}}
  \State \texttt{DataBuf} $\gets$ \Call{decompress}{\texttt{CompDataBuf}}
  \State \texttt{offs} $\gets$ \Call{CalculateOffset}{\texttt{CN}}
  \State \Call{Iwrite\_at}{\texttt{DataBuf}, \texttt{offs}, \texttt{WriteReq}}
  \State \Call{wait}{\texttt{RecvReq}}
\EndWhile

\end{algorithmic}
}
\end{algorithm}


\begin{algorithm}
\caption{Compute Node Write Phase}
\label{alg:cnwrite}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{NumSeriesFinished} $\gets$ 0
\For{\texttt{s} = 1 \textbf{to} \texttt{NumSeries}}
  \State \texttt{SeriesIndices}[\texttt{s}] $\gets$ 0
  \State \texttt{SeriesFinished}[\texttt{s}] $\gets$ $false$
  \State \Call{send}{\texttt{s}, $TO\_MY\_CDN$}
  \State \texttt{RecvBufs}[\texttt{s}] $\gets$ \Call{Irecv}{$FROM\_MY\_CDN$, \texttt{RecvReqs}[\texttt{s}]}
\EndFor

\State \Comment Main Loop
\While{\texttt{NumSeriesFinished} < \texttt{NumSeries}}
  \For{\texttt{i} = 1 \textbf{to} \texttt{MergeBuf.size}}
    \State \texttt{min} $\gets$ $inf$
    \For{\texttt{s} = 1 \textbf{to} \texttt{NumSeries}}
      \If{\texttt{SeriesFinished}[\texttt{s}]} \textbf{continue} \EndIf
      \State \texttt{si} $\gets$ \texttt{SeriesIndices}[\texttt{s}]
      \If{\texttt{si} > \texttt{SeriesBufs}[\texttt{s}]\texttt{.length}}
        \State \Call{wait}{\texttt{RecvReqs}[\texttt{s}]}
        \If{received series finisher}
          \State \texttt{NumSeriesFinished} $\gets$
          \State \texttt{ NumSeriesFinished} + 1
          \State \texttt{SeriesFinished}[\texttt{s}] $\gets$ $true$
          \State \textbf{continue}
        \EndIf
        \State \Call{swap}{\texttt{RecvBufs}[\texttt{s}], \texttt{CompSeriesBufs}[\texttt{s}]}
        \State \Call{send}{\texttt{s}, $TO\_MY\_CDN$}
        \State \texttt{RecvBufs}[\texttt{s}] $\gets$ 
        \State \ \ \ \ \Call{Irecv}{$FROM\_MY\_CDN$, \texttt{RecvReqs}[\texttt{s}]}
        \State \texttt{SeriesBufs}[\texttt{s}] $\gets$
        \State \ \ \ \ \Call{decompress}{\texttt{CompSeriesBufs}[\texttt{s}]}
        \State \texttt{si} $\gets$ 1
        \State \texttt{SeriesIndices}[\texttt{s}] $\gets$ 1
      \EndIf
      \If{\texttt{SeriesBufs}[\texttt{s}][\texttt{si}] < min}
        \State \texttt{min} $\gets$ \texttt{SeriesBufs}[\texttt{s}][\texttt{si}]
        \State \texttt{minseries} $\gets$ \texttt{s}
      \EndIf
    \EndFor
    \If{\texttt{NumSeriesFinished} = \texttt{NumSeries}} \textbf{break} \EndIf
    \State \texttt{MergeBuf}[\texttt{i}] $\gets$ \texttt{min}
    \State \texttt{SeriesInidices}[\texttt{minseries}] $\gets$
    \State \ \ \ \ \texttt{SeriesInidices}[\texttt{minseries}] + 1
  \EndFor
  \If{\texttt{MergeBuf.length} = 0} \textbf{break} \EndIf
  \State \Call{wait}{\texttt{SendReq}}
  \State \Call{swap}{\texttt{MergeBuf}, \texttt{SendBuf}}
  \State \texttt{CompSendBuf} $\gets$ \Call{compress}{\texttt{SendBuf}}
  \State \Call{Isend}{\texttt{CompSendBuf}, $TO\_MY\_SDN$, \texttt{SendReq}}
  \State \texttt{MergeBuf.clear()}
\EndWhile

\end{algorithmic}
}
\end{algorithm}

\begin{algorithm}
\caption{Compute-side Data Node Write Phase}
\label{alg:cdnwrite}
{\fontsize{8}{15}
\begin{algorithmic}[1]

\State \Comment Initialization
\State \texttt{finished} $\gets$ 0
\For{each compute node \texttt{CN} assigned to this \texttt{CDN}}
  \State \texttt{ReqSeries}[\texttt{CN}] $\gets$ \Call{Irecv}{\texttt{CN}, \texttt{RecvReqs}[\texttt{CN}]}
  \State \texttt{RemainingSeries}[\texttt{CN}] $\gets$ \texttt{FileIndex}[\texttt{CN}]\texttt{.length}
  \For{\texttt{s} = 1 \textbf{to} \texttt{RemainingSeries}[\texttt{CN}]}
    \State \texttt{NextChunk}[\texttt{CN}][\texttt{s}] $\gets$ 0
  \EndFor
\EndFor

\State \Comment Main Loop
\While{\texttt{finished} < \texttt{assigned}}
  \State \Call{waitany}{\texttt{RecvReqs}}
  \State \texttt{CN} $\gets$ \texttt{RecvReqs.source}
  \State \texttt{series} $\gets$ \texttt{ReqSeries}[\texttt{CN}]
  \State \texttt{chunkNo} $\gets$ \texttt{NextChunk}[\texttt{CN}][\texttt{s}]
  \If{\texttt{chunkNo} > \texttt{FileIndex}[\texttt{CN}][\texttt{series}].length}
    \State \Call{SendSeriesFinisher}{\texttt{CN}}
    \State \texttt{RemaingSeries}[\texttt{CN}] $\gets$
    \State \ \ \ \ \texttt{RemaingSeries}[\texttt{CN}] - 1
    \If{\texttt{RemaingSeries}[\texttt{CN}] = 0}
      \State \texttt{finished} $\gets$ \texttt{finished} + 1
    \Else
      \State \texttt{ReqSeries}[\texttt{CN}] $\gets$ \Call{Irecv}{\texttt{CN}, \texttt{RecvReqs}[\texttt{CN}]}
    \EndIf
    \State \textbf{continue}
  \EndIf
  \State \Call{wait}{\texttt{SendReq}}
  \State \texttt{SendBuf} $\gets$ \Call{ReadFromLocalFile}{\texttt{CN}, \texttt{series}, \texttt{chunkNo}}
  \State \Call{Isend}{\texttt{SendBuff}, \texttt{CN}, \texttt{SendReq}}
  \State \texttt{NextChunk}[\texttt{CN}][\texttt{series}] $\gets$ \texttt{chunkNo} + 1
  \State \texttt{ReqSeries}[\texttt{CN}] $\gets$ \Call{Irecv}{\texttt{CN}, \texttt{RecvReqs}[\texttt{CN}]}
\EndWhile
\end{algorithmic}
}
\end{algorithm}

Each CN merges data from all the series it produced during the write phase into a merge buffer (represented by the \texttt{MergeBuf} buffer in the pseudocode). Initially, the CN requests the first chunk of each series and then it performs a multi-way merge of all these chunks (lines 11-36 of algorithm~\ref{alg:cnwrite}). If all the elements of a chunk have been merged into the result, the next chunk of these series is requested (lines 15-29 of algorithm~\ref{alg:cnwrite}). When the merge buffer is full, it is compressed and sent to the appropriate SDN. The time spent in compressing and sending is overlapped with the rest of the operations. Also, the time for receiving a new chunk is overlapped with the process of merging. The stream of merge buffers being sent contains all the input data belonging to the CN's subrange in sorted order.


Each SDN receives sorted data buffers from its assigned CNs, decompresses it and stores them in the disk using non-blocking MPI-IO writes. The SDNs are responisble for storing data received from different CNs in the correct order according to CNs' subranges. The combined decompressing and writing time is overlapped with the time between subsequent receives from CNs.
