\section{Performance Modeling}
\label{sec:model}
\START
In this section we devise a simple performance model for the read phase of SDNs. This is only one component out of the six components of our algorithm. Due to lack of space, we ignore the performance model of other five components, but those can be derived in a similar way. Derived performance model for all the components can be found in the source code repository of the project. Our analysis is based on system and algorithm specific parameters shown in Table~\ref{table:systemmodelparams} and Table~\ref{table:tunablemodelparams}. Note that we assume zero network latency in our modeling as almost all messages in our algorithm are large.

For \textbf{SDNs}, there are four major operations:
\vspace{-3.5mm}
\begin{itemize}
\item Reading a buffer of size SDN\_READ\_SIZE from the file system.
\item Sorting the read buffer and merging it with the in\-/memory buffer (with maximum size of SDN\_BUFFER\_\-SIZE). We used quicksort in parallel where each thread performs approximately 1.5 accesses per element. For merging, two memory accesses per element are required.
\vspace{-6mm}
\item Compressing consecutive ranges of the in-memory sorted buffer.
\vspace{-2mm}
\item Sending compressed ranges to the CNs.
\end{itemize}
\RM
\begin{displaymath}
T_{read}=\texttt{PFS\_LATENCY}+\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{PFS\_BW}}
\end{displaymath}
\RM
\begin{dmath*}
T_{sort} = \left(1.5\times\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{CORES\_PER\_NODE}}log\left({\frac{\texttt{SDN\_READ\_SIZE}}{\texttt{TYPE\_SIZE}\times\texttt{CORES\_PER\_NODE}}}\right)
+ 2\times\texttt{SDN\_READ\_SIZE} + 2\times\texttt{SDN\_BUFFER\_SIZE}\right)\times\frac{1}{ \texttt{MEMORY\_BW}}
\end{dmath*} 
\RM
\begin{dmath*}
T_{comp} = \frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{COMPRESSION\_BW}\times\texttt{CORES\_PER\_NODE}}
\end{dmath*}
\RM
\begin{dmath*}
T_{send} = \frac{\texttt{SDN\_BUFFER\_SIZE} \times \texttt{WCCR}}{\texttt{NETWORK\_BW}}
\end{dmath*}

Based on the algorithm, $T_{read}$ is overlapped with $T_{sort}$. Once the in-memory buffer is full (after $\frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{SDN\_READ\_SIZE}}$ rounds of reading/sorting), compression happens. All of these operations are overlapped with $T_{send}$. Therefore, the time it takes for SDN from the moment it starts reading data to the moment it sends out messages to CNs (which we call SDN's gap) can be expressed as follows:
\begin{dmath*}
SDN_{gap}^{read} = max\left\{\frac{\texttt{SDN\_BUFFER\_SIZE}}{\texttt{SDN\_READ\_SIZE}} \times max \left\{ T_{read}, T_{sort} \right\}
 + T_{comp}, T_{send}\right\}
\end{dmath*}
The total effective execution time of each SDN in read phase is the sum of all these gaps for all communication rounds where SDN sends data to CNs. Hence, the total effective execution time of SDNs can be expressed as:
\begin{dmath*}
T_{SDN}^{read}=\frac{\texttt{FILE\_SIZE}}{\texttt{NUM\_SDN}\times\texttt{SDN\_BUFFER\_SIZE}}\times SDN_{gap}^{read}
\end{dmath*}

One can repeat the same analysis for the other five components of our sorting algorithm and find the effective execution time of each component. Total execution time of the algorithm, then, can be expressed as follows:
\begin{dmath*}
T=max\left\{T_{SDN}^{read},T_{CDN}^{read},T_{CN}^{read}\right\}+max\left\{T_{SDN}^{write},T_{CDN}^{write},T_{CN}^{write}\right\}
\end{dmath*}