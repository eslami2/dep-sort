#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

using namespace std;

void readFromFile(unsigned int *buff, long offset, size_t count, FILE *file) {
  fseek(file, offset, SEEK_SET);

  size_t read = 0;
  while (read < count) {
    size_t more = fread(buff + read, 1, count - read, file);
    read += more;
  }
}

int main(int argc, char** argv) {

  FILE *file = fopen(argv[1], "rb");
  int SIZE = atoi(argv[2]);
  unsigned int* data = (unsigned int*) malloc(SIZE * sizeof(unsigned int));
  readFromFile(data, 0, SIZE * sizeof(int), file);
  int error = 0;
    
  for (int i = 0; i < SIZE; ++i) cout << data[i] << " ";
  cout << endl;
  for (int i=0; i<SIZE - 1; ++i) {
    if (data[i] > data[i+1]) {
      cout << "data[" << i << "] <= data[" << i + 1 << "] failed: " << data[i]
           << " <= " << data[i + 1] << endl;
      error = 1;
    }
  }
  free(data);
  return error;
}
