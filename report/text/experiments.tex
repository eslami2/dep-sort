\section{Experiments}
\label{sec:experiments}

In this section we describe our choice of infrastructure to run our implementation, give an overview on micro-benchmarks chosen to find critical system parameter, and demonstrate tuning and performance results of our implementation.

\subsection{System Setup}
Since our design is based on DEP system architecture, to get the most benefit out of our design, we have to choose an HPC testbed that provides certain capabilities. DEP architecture promises the close proximity of Storage-side Data Nodes (SDNs) to file-servers. Also, we based our design on availability of SSD in a few nodes close to Compute Nodes (CNs). Furthermore, in order to see the benefit of data reduction in our design, the testbed should provide a limited bandwidth between SDNs and CNs. We chose to run our implementation on IIT-HEC data-intensive HPC cluster. Figure~\ref{fig:hectopology} depicts the topology of this cluster. Each node is equipped with a Quad-core AMD Opteron 1.6Ghz with 4MB of L3 cache that supports up to 8 independent integer threads working simultaneously at 800Ghz. 
A selected number of nodes have SSD along with HDD.

\begin{figure}[t]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/hectopo.png}
    \caption{Topology of the experiment cluster.}
    \label{fig:hectopo}
\end{figure}

We setup OrangeFS/PVFS2~\cite{pvfs} to run on 8 nodes of switch 0. The PVFS2 uses HDD of the 8 assigned nodes to stripe the data. In order to mimic the DEP architecture, we choose SDNs amongst available nodes of switch 0, and we choose Compute-side Data Nodes (CDNs) and CNs amongst nodes in switch 1 and 2. We also compare our design to BigSort (Integer sorting in CORAL benchmark~\cite{coral}). We use the same PFVS2 configuration for BigSort. However, compute nodes for running BigSort is chosen amongst nodes in switch 1 and 2.

\subsection{Micro-benchmarking and System Parameters}
Here is the list of system parameters and a description of the micro-benchmark we ran to find out about the parameter:
\begin{itemize}
  \item \textbf{Access time to parallel file storage and also to SSD.} This is achieved by measuring the time it takes to write/read a small portion to/from a random location of a file.
  \item \textbf{Bandwidth to parallel file storage and also to SSD.} This is achieved by running ioda.c (given in HW10 in the course) and considering the level-3 collective IO write as the bandwidth to disk. Note that if we run the benchmark on nodes within the same switch of PVFS2, we get 162 MiB/s bandwidth, while if we run the benchmark from another switch the bandwidth is 111 MiB/s. In the later case limited network bandwidth between two switches causes a reduction of the effective bandwidth seen by the compute nodes. The result of this benchmark even more signifies the benefits of DEP architecture. In general, it is expected that the SDNs get a higher bandwidth to parallel file system due to their proximity.
  \item \textbf{Memory bandwidth.} We ran STREAM to find this out.
  \item \textbf{Compression and decompression bandwidth.} We ran the compression algorithm on 128 MiB of data to find this out.
\end{itemize}
Table~\ref{table:systemmodelparams} contains the full list of values of system parameters obtained by running micro-benchmarks.

\begin{table}[t]
\caption{System Model Parameters (Result of Micro-Benchmarks)}
\centering
\begin{tabular}{l l}
\hline \hline
Parameter & Value \\ [0.5ex]
\hline
PFS\_LATENCY        &  2.8 ms\\
PFS\_BW             &  111/162 MiB/s\\
SSD\_LATENCY        &  5 us\\
SSD\_BW             &  368 MiB/s\\
STREAM\_BW          &  2.8 GiB/s\\
NETWORK\_BW         &  10.9 MiB/s\\
COMPRESSION\_BW   & 520 MiB/s \\
DECOMPRESSION\_BW & 604 MiB/s \\
WCCR                &  0.3-0.5\\ [1ex]
\hline
\end{tabular}
\label{table:systemmodelparams}
\end{table}
 
 
\subsection{Performance Tuning}
There are two angles for performance tuning: tuning for buffer sizes, and tuning for ratio of number of CNs to CDNs and SDNs. We used the performance modeling to tune the size of various buffers we have in different types of nodes. Note that this tuning considers the memory requirement of each type of node and the restriction that the total amount of memory usage in each node should not exceed DRAM\_ALLOCATION. Table~\ref{table:tunablemodelparams} contains the values of the system parameters, specially buffer sizes that is suggested by the performance model.

\begin{table}[t]
\caption{Tunable and Derived Model Parameters}
\centering
\begin{tabular}{l l}
\hline \hline
Parameter & Value \\ [0.5ex]
\hline
NUM\_FS             &  1,2,4\\
NUM\_IO             &  1,2,4\\
NUM\_CN             &  4,8,12,16,20,24\\
CORES\_PER\_NODE    &  8\\
DRAM\_ALLOCATION    &  1 GiB\\
FS\_BUFFER\_SIZE   &  32,64,128 MiB\\
FS\_READ\_SIZE     &  16,32 MiB\\
CN\_MERGE\_BUFFER  &  200 MiB\\
CHUNK\_SIZE        &  16,20 MiB\\
CN\_MMERGE\_BUFFER &  eq. to FS\_BUFFER\_SIZE\\
TYPE\_SIZE          &  4 bytes (integers)\\
FILE\_SIZE          &  $2 \times (NUM\_FS + NUM\_IO+$\\
~  & $ NUM\_CN)$ GiB\\
CN\_RECV\_BUFFER &  eq. to \footnotesize{$\frac{FS\_BUFFER\_SIZE}{NUM\_CN}$}\\
NUM\_CHUNKS      &  eq. to \footnotesize{$\frac{CN\_MERGE\_BUFFER}{CHUNK\_SIZE}$}\\
NUM\_SERIES      &  eq. to \footnotesize{$\frac{FILE\_SIZE}{NUM\_CN \times CN\_MERGE\_BUFFER}$}\\ [1ex]
\hline
\end{tabular}
\label{table:tunablemodelparams}
\end{table}


Figure~\ref{fig:tuning} shows the effect of number of CDNs and SDNs on the performance. It is clear that the more CDNs or SDNs results in the better performance. In the extreme case where number of CNs, CDNs, and SDNs are all equal, each SDN/CDN is responsible to serve one and only one CN. This causes the minimum amount of contention on CDNs, and maximizes the parallelization. However, in the real larger-scale experiments, CNs are the most numerous resources available in a system, hence matching up the number of CDNs and SDNs to number of CNs is not possible. Because of this reason, for the rest of our experiments, on the fairly small scale IIT-HEC cluster, we assume that the number of SDNs and also the number of CDNs is 4. Note that the performance model and the actual execution numbers are very close. This is helpful to predict the optimum number of CNs, CDNs, and SDNs on a cluster for each run of our sort algorithm.

\begin{figure*}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/iof_effect}
  \caption{Effect of number of CDNs (with 4 CN, 4 SDN)}
  \label{fig:iof_effect}
\end{subfigure}%
~
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/fs_effect}
  \caption{Effect of number of SDN (with 4 CN, 4 CDN)}
  \label{fig:fs_effect}
\end{subfigure}
\caption{Effect of different number of CDNs and SDNs on performance. File size in all these experiments was 16 GiB.}
\label{fig:tuning}
\end{figure*}

\subsection{Scalability Experiments and Comparison with BigSort}
Figure~\ref{fig:sca} depicts the weak scalability graph for execution time and sorting rate for our implementation and BigSort. Note that the total number of nodes for DEPSort is the sum of number of CNs, CDNs, and SDNs. 

First, the performance model matches almost perfectly with the timing from actual experiments. This means the performance model is accurate enough and and can be used to predict the execution time of our sort algorithm on a DEP cluster.

Second, the average rate at which our algorithm sorts the data is 71 MB/s. Considering that the PVFS2 bandwidth is 162 MB/s, and considering the fact that for sorting the data we have to at least read the entire raw date once and write the entire sorted data back to the disk, the best achievable rate is 81 MB/s. This means our soring implementation overlaps almost entire computation and communication with just reading the raw data and writing back the sorted data.

Third, DEPSort is 4.4X faster than BigSort on average. This is due to the entire overlap of communication and computation with disk operation in DEPSort, and also the fact that DEPSort compresses the intermediate results and stores them in fast SSDs. In case of BigSort, the intermediate results are written to the parallel file system, and also the actual sorting algorithm reads and writes the input multiple times.

Lastly, performance of DEPSort gradually drops at larger number of nodes. This is mainly due to the contention over CDNs and SDNs. In the largest case, we have 20 CNs, 4 CDNs, and 4 SDNs. That is the ratio of 5 for number of CNs over CDNs/SDNs. Due to the recourse limitations in our experiments we were not able to run the algorithm on larger scale. However, if we could run on larger scale, we would have been able to find the best ratio of CNs over CDNs/SDNs in which the performance is still acceptable.

\begin{figure*}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/sca_time}
  \caption{Sort Execution Time}
  \label{fig:sca_time}
\end{subfigure}%
~
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{figures/sca_rate}
  \caption{Sort Rate}
  \label{fig:sca_rate}
\end{subfigure}
\caption{Weak scaling result of DEPSort and BigSort. For DEPSort, total number of nodes is equal to the sum of number of CNs, CDNs, and SDNs. Also, for DEPSort experiments, we use 4 CDNs and 4 SDNs. DRAM\_ALLOCATION for both DEPSort and BigSort is 1 GiB. For each experiment, the file size is twice the aggregate memory allocation on the entire nodes (i.e. for 12 nodes we use 24 GiB files, for 16 nodes we use 32 GiB files, and so on.)}
\label{fig:sca}
\end{figure*}
